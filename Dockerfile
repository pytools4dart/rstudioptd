# Build with BuildKit: DOCKER_BUILDKIT=1
FROM rocker/geospatial:4.1.2

# avoid userconf script to recursively change owner of mounted volumes at start
# and set bash as default shell (useradd used in /etc/cont-init.d/userconf)
RUN sed -i 's/chown -R/chown/g' /etc/cont-init.d/02_userconf \
    && sed -i "s:SHELL=/bin/sh:SHELL=/bin/bash:" /etc/default/useradd

# add skeleton if files do not exist
COPY userconfbash.sh /etc/cont-init.d/userconfbash
COPY rstudio-prefs.json /etc/rstudio/rstudio-prefs.json

# install x2go server
RUN apt update \
  && DEBIAN_FRONTEND=noninteractive TZ=Europe/Paris apt install -y openssh-server xfce4 xfce4-goodies x2goserver x2goserver-xsession \
  && apt clean -y
EXPOSE 22
COPY userconfx2go.sh /etc/cont-init.d/userconfx2go

# add conda (for members of group staff)
# ARG MAMBA_VERSION=4.12.0-0
ARG MINICONDA_PATH=/opt/miniconda
ENV PATH=${MINICONDA_PATH}/condabin:${MINICONDA_PATH}/bin:${PATH}
RUN curl -L https://github.com/conda-forge/miniforge/releases/latest/download/Mambaforge-Linux-x86_64.sh -o /tmp/miniforge.sh \
    && mkdir -p /opt \
    && sh /tmp/miniforge.sh -b -p ${MINICONDA_PATH} \
    && rm /tmp/miniforge.sh \
    && ln -s ${MINICONDA_PATH}/etc/profile.d/conda.sh /etc/profile.d/conda.sh \
    && find ${MINICONDA_PATH}/ -follow -type f -name '*.a' -delete \
    && find ${MINICONDA_PATH}/ -follow -type f -name '*.js.map' -delete \
    && conda update -y conda \
    && conda clean -afy \
    && chgrp -R staff ${MINICONDA_PATH} \
    && chmod g+rwx -R ${MINICONDA_PATH} \
    && echo ". ${MINICONDA_PATH}/etc/profile.d/conda.sh" >> /etc/skel/.bashrc \
    && echo "conda activate base" >> /etc/skel/.bashrc \
    && echo ". ${MINICONDA_PATH}/etc/profile.d/conda.sh" >> /root/.bashrc \
    && echo "conda activate base" >> /root/.bashrc


# add renv
ARG RENV_VERSION=0.15.0 # current default version
ENV RENV_PATHS_CACHE=/renv/cache
RUN R -e "devtools::install_version('renv', '${RENV_VERSION}')" \
    && echo "RENV_PATHS_CACHE=${RENV_PATHS_CACHE}" >> ${R_HOME}/etc/Renviron
# see https://github.com/aforsythe/r-seurat/blob/main/Dockerfile to set RENV_CACHE as mounted volume
# maybe it could be mounted as docker cache volume...

# add reticulate
RUN R -e "install.packages('reticulate')" \
    && echo "RETICULATE_MINICONDA_PATH=${MINICONDA_PATH}" >> ${R_HOME}/etc/Renviron

# add pytools4dart
ARG PYTOOLS4DART_VERSION="master"
RUN curl -L https://gitlab.com/pytools4dart/pytools4dart/-/raw/${PYTOOLS4DART_VERSION}/environment.yml?inline=false -o /tmp/environment.yml \
    && mamba env create --name ptdvenv -f /tmp/environment.yml -v \
    && rm /tmp/environment.yml \
    && chgrp -R staff ${MINICONDA_PATH}/envs/ptdvenv \
    && chmod g+rwx -R ${MINICONDA_PATH}/envs/ptdvenv \
    && sed -i 's/conda activate base/conda activate ptdvenv/' /etc/skel/.bashrc \
    && sed -i 's/conda activate base/conda activate ptdvenv/' /root/.bashrc \
    && echo ". ${MINICONDA_PATH}/etc/profile.d/conda.sh" >> /home/rstudio/.bashrc \
    && echo "conda activate ptdvenv" >> /home/rstudio/.bashrc \
    && echo "RETICULATE_MINICONDA_ENVNAME=ptdvenv" >> ${R_HOME}/etc/Renviron # sets ptdvenv as default python

RUN
SHELL ["conda", "run", "-n", "ptdvenv", "/bin/bash", "--login", "-c"]
# add DART **Warning** user_data will be in docker image, to link use option -e "USER_DATA=path/to/user_data"
# rgeos is also reinstalled otherwise conda and R GEOS version are different
# SHELL ["/bin/bash", "--login", "-c"]
# RUN ${MINICONDA_PATH}/bin/conda init bash
SHELL ["/bin/bash", "--login", "-c"]
ENV DART_PREFIX=/opt/DART
RUN --mount=type=bind,source=dart.tar.gz,target=/tmp/dart.tar.gz \
    R -e "devtools::install_git('https://github.com/cran/rgeos.git', build=T)" && \
    conda activate ptdvenv && \
    python -c "from pytools4dart.dart import install; install(r'/tmp/dart.tar.gz', r'${DART_PREFIX}', verbose=True)" && \
    mv /root/.dartrc* /root/.pytools4dartrc /etc/skel/ && \
    sed -i 's|root|etc/skel|' ${DART_PREFIX}/config.ini && \
    chgrp -R staff ${DART_PREFIX} && \
    chmod g+rwx -R ${DART_PREFIX}
COPY userconfdart.sh /etc/cont-init.d/userconfdart

# What the f...
RUN sed -i 's/echo "HELLO FROM ACTIVATE.SH"//' /opt/miniconda/envs/ptdvenv/etc/conda/activate.d/pyjnius_activate.sh


# CMD ["/bin/sh", "-c", "/usr/bin/ssh-keygen -A && /usr/sbin/sshd -D"]
