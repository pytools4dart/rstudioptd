#!/usr/bin/with-contenv bash

## Set defaults for environmental variables in case they are undefined
DEFAULT_USER=${DEFAULT_USER:-rstudio}
USER=${USER:=${DEFAULT_USER}}

if [ ! -f /home/$USER/.bashrc ]
  then 
    cp /etc/skel/.bashrc /home/$USER/ 
    chown $USER:$USER /home/$USER/.bashrc
fi

if [ ! -f /home/$USER/.bash_logout ]
  then 
    cp /etc/skel/.bash_logout /home/$USER/ 
    chown $USER:$USER /home/$USER/.bash_logout
fi

if [ ! -f /home/$USER/.profile ]
  then 
    cp /etc/skel/.profile /home/$USER/ 
    chown $USER:$USER /home/$USER/.profile
fi

