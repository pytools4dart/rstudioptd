#!/usr/bin/with-contenv bash

## Set defaults for environmental variables in case they are undefined
DEFAULT_USER=${DEFAULT_USER:-rstudio}
USER=${USER:=${DEFAULT_USER}}

DEFAULT_USER_DATA=${DART_PREFIX}/user_data
USER_DATA=${USER_DATA:=${DEFAULT_USER_DATA}}

if [ ! -f /home/$USER/.dartrc* ]
  then 
    cp /etc/skel/.dartrc* /home/$USER/
    chown $USER:$USER /home/$USER/.dartrc*
fi

if [ ! -f /home/$USER/.pytools4dartrc ]
  then
    cp /etc/skel/.pytools4dartrc /home/$USER/
    chown $USER:$USER /home/$USER/.pytools4dartrc
fi

# link to user_data if exist or reverse if it doesn't
# use docker run option -e <USER_DATA> to do so
if [ "$USER_DATA" != "$DEFAULT_USER_DATA" ]
  then
    if [ -d ${USER_DATA} ]
    then
      rm -rf ${DART_PREFIX}/user_data
    else
      mkdir -p $(dirname ${USER_DATA})
      mv ${DEFAULT_USER_DATA} ${USER_DATA}
    fi
    ln -s ${USER_DATA} ${DART_PREFIX}/user_data
fi
      