This docker includes:
- an rstudio-server with geospatial packages and renv in order to control R environment,
- an x2go server in order to access with xfce interface,
- python3 and a conda environment named `ptdvenv` including the dependencies of `pytools4dart`.

An old version of DART is included (v1142), [registration](https://dart.omp.eu/#/getDart) is free but mandatory 
in order to have the most recent DART version.

# Install
Pull the docker image and create an image defining the wanted ports, e.g. with port numbers 2222 for x2go and 8785 for rstudio-server:
```
docker pull registry.gitlab.com/pytools4dart/rstudioptd/rstudioptd
# Rename image to have a simpler name (optional)
docker tag registry.gitlab.com/pytools4dart/rstudioptd/rstudioptd rstudioptd
# Run an instance (container) of the image `rstudioptd`
docker run -it --name myrstudioptd -p 2222:22 -p 8785:8787 rstudioptd
```
Use option `--rm` in order to remove automatically the container when stopped.

From the host, the created docker can be accessed
- with rstudio-server : from a browser with url `localhost:8785`, with default username `rstudio` and password given in command line output 
- with x2goclient: configure server `localhost`, port `2222` and session type `XFCE`.

Access from remote replacing `localhost` with the IP address of the host computer.

Sections below are listing other options available.

## Add user
As a default, user and group are both named `rstudio`, with user and group id `1000`.
The default password is displayed in the terminal from which the container is created, or is available in the docker container log.
If other values are wanted, typically to keep permissions as in host session, it can be changed using environment variable:

```shell
docker run -it --name myrstudioptd -p <x2go host port>:22 -p <rstudio host port>:8787 \
-e "USER=<user account>" -e "PASSWORD=<password>" -e "USERID=<user id>" -e "GROUPID=<group id>" rstudioptd
```
Warning: password will be visible in the docker container configuration,
thus it is recommended to redefine it, e.g. from the rstudio terminal with command line with `passwd`

## Data volumes
In order to use host data, volumes must be mounted using `-v` option.
The following example defines the host user and group names and ID numbers, and mounts the data volume on current user's
home directory at the same location inside docker:
```shell
docker run -it --name myrstudioptd -p <x2go host port>:22 -p <rstudio host port>:8787 \
-e "USER=$USER" -e "PASSWORD=$USER" -e "USERID=$(id -u)" -e "GROUPID=$(id -g)" -v $HOME/data:$HOME/data rstudioptd
```

## Specify DART user_data directory
Use docker environment variable `USER_DATA`, example:
```shell
docker run -it --name myrstudioptd -p <x2go host port>:22 -p <rstudio host port>:8787 \
-e "USER=$USER" -e "PASSWORD=$USER" -e "USERID=$(id -u)" -e "GROUPID=$(id -g)" -v $HOME/data:$HOME/data \
-e 'USER_DATA=${HOME}/data/my_user_data' rstudioptd
```

## Restart docker container
If docker stops (e.g. after a computer restart), it can be restarted with command
```shell
docker start myrstudioptd
```

## Install or update DART
In order to install or change DART version, download a linux `.tar.gz` archive of DART.

Download can be done several ways:
- using firefox within x2go
- downloading on your local machine and uploading file to docker with rstudio-server

Then, install:
- from rstudio console:
```r
library(reticulate)
ptd=import('pytools4dart')
ptd$dart$install('<DART archive .tar.gz>', '~/DART', verbose=T)
```

- from a terminal (on rstudio or within x2go session):
```shell
python -c "from pytools4dart.dart import install; install(r'<DART archive .tar.gz>', r'~/DART', verbose=True)"
```

# Docker tips
A few docker commands:
- stop container : `docker stop myrstudioptd`
- restart container: `docker start myrstudioptd`
- remove container: `docker rm myrstudioptd`, **it would remove all that was installed manually inside the container, e.g. new packages**

# Build docker
If one needs to rebuild the image, e.g. to change R version:
1. clone repo
2. download a linux DART archive from https://dart.omp.eu/#/getDart into the root directory of the cloned repo and rename it `dart.tar.gz`
3. make modifications in Dockerfile
4. from the root directory build with the following command line:
```
DOCKER_BUILDKIT=1 docker build -t rstudioptd .
```
